import { Component } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import countries from './countries';

export interface StepType {
  label: string;
  header: string,
  fields: FormlyFieldConfig[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  activedStep = 0;

  model = {};
  steps: StepType[] = [
    {
      label: 'Contact Info',
      header: 'We want to know you better',
      fields: [
        {
          fieldGroupClassName: 'display-flex',
          fieldGroup: [
            {
              template: '<hr /> <p><strong>Legal Name:</strong></p><p><i>Please provide your full Legal name. This will help us when we collect your admission documents.</i></p>',
            },
            {
              type: 'input',
              key: 'firstName',
              templateOptions: {
                label: 'First Name',
                appearance: 'outline'
              },
            },
            {
              type: 'input',
              key: 'middleName',
              templateOptions: {
                label: 'Middle Name',
                appearance: 'outline'
              },
            },
            {
              type: 'input',
              key: 'lastName',
              templateOptions: {
                label: 'Last Name',
                appearance: 'outline'
              },
            },
          ],
        },
        {
          template: '<hr /><p>Do you have any previous legal names? (optional)</p><p><i>If you have any previous names, such as a maiden name, please let us know. It helps when requesting transcripts from previous schools where your name might be different.</i></p>',
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              type: 'radio',
              key: 'previousName',
              templateOptions: {
                options: [
                  {
                    label: 'Yes ',
                    value: 'yes',
                  },
                  {
                    label: 'No',
                    value: 'no',
                  }
                ],
              },
            },
          ],
        },
        {
          template: '<hr /><p>Enter your email address:</p><p><i>By entering your email address you are consenting to the University sending  you notifications, updates, requests for information, and/or marketing information concerning University of Phoenix.</i><p>',
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              type: 'input',
              key: 'email',
              templateOptions: {
                label: 'Email',
                appearance: 'outline'
              },
            },
          ],
        },
        {
          template: '<hr /> <div><strong>Address:</strong></div>',
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              className: 'flex-2',
              type: 'input',
              key: 'address',
              templateOptions: {
                label: 'Address: ',
                appearance: 'outline'
              },
            },
            {
              className: 'flex-2',
              type: 'input',
              key: 'street',
              templateOptions: {
                label: 'Country: ',
                appearance: 'outline'
              },
            },
            {
              className: 'flex-2',
              type: 'input',
              key: 'addressLine1',
              templateOptions: {
                label: 'Address Line 1: ',
                appearance: 'outline'
              },
            },
            {
              className: 'flex-2',
              type: 'input',
              key: 'addressLine2',
              templateOptions: {
                label: 'Address Line 2: ',
                appearance: 'outline'
              },
            },
            {
              className: 'flex-2',
              type: 'input',
              key: 'city',
              templateOptions: {
                label: 'City',
                appearance: 'outline'
              },
            },
            {
              className: 'flex-2',
              type: 'input',
              key: 'state',
              templateOptions: {
                label: 'State/Province',
                appearance: 'outline'
              },
            },
            {
              className: 'flex-2',
              type: 'input',
              key: 'postalCode',
              templateOptions: {
                label: 'Postal Code',
                appearance: 'outline'
              },
            },
          ],
        },
        {
          template: '<hr /><p><strong>Phone:</strong></p>'
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              key: 'type',
              type: 'select',
              templateOptions: {
                options: [
                  { label: 'Mobile', value: 'mobile' },
                  { label: 'Home', value: 'home' },
                  { label: 'Work', value: 'work' },
                ],
                appearance: 'outline'
              },
            },
            {
              key: 'location',
              type: 'select',
              templateOptions: {
                options: [
                  { label: 'United States (+1)', value: 'unitedStates' },
                  { label: 'Canada (+2)', value: 'canada' },
                  { label: 'Somewhere Else (+3)', value: 'elsewhere' },
                ],
                appearance: 'outline'
              },
            },
            {
              key: 'phoneNumber',
              type: 'input',
              templateOptions: {
                label: 'Phone Number',
                appearance: 'outline'
              },
            },
          ],
        },
      ]
    },
    {
      header: 'We need a little more personal information',
      label: 'Tell Us More',
      fields: [
        {
          fieldGroupClassName: 'display-flex',
          fieldGroup: [
            {
              type: 'datepicker',
              key: 'dateOfBirth',
              templateOptions: {
                label: 'Date of Birth',
                appearance: 'outline'
              },
            },
          ],
        },
        {
          template: `
            <hr />
            <p>What's your Social Security Number (SSN)?</p>
            <p><i>Your Social Security Number helps us collect your transcripts and is needed to process financial aid and some military and veteran benefits. It will also assist the University in preparing important tax documents to help you when you file income taxes in the future.</i></p>
          `,
        },
        {
          fieldGroupClassName: 'd-flex flex-wrap',
          fieldGroup: [
            {
              type: 'input',
              key: 'ssn',
              templateOptions: {
                label: 'Social Security Number',
                appearance: 'outline'
              },
            },
            {
              className: 'row',
              type: 'checkbox',
              key: 'nonDisclosure',
              templateOptions: {
                label: `I don't have an SSN or I choose not to disclose it.`,
              },
            },
          ],
        },
        {
          template: `<hr> <p>Are you a U.S. citizen?</p>`,
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              type: 'radio',
              key: 'usCitizen',
              templateOptions: {
                options: [
                  {
                    label: 'Yes ',
                    value: 'yes',
                  },
                  {
                    label: 'No',
                    value: 'no',
                  }
                ],
              },
            },
          ],
        },
        {
          template: `<hr /><p>What's your country of citizenship?</p>`
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              key: 'citizenshipCountry',
              type: 'select',
              templateOptions: {
                options: countries,
                appearance: 'outline'
              },
            },
          ]
        },
        {
          template: `<hr /><p>What's your current U.S. Visa Type, Permanent Resident or Other?</p>`
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              key: 'visaType',
              type: 'select',
              templateOptions: {
                options: [
                  { label: 'Permanent Resident', value: 'permanent_visa' },
                  { label: 'Refugee', value: 'refugee' },
                  { label: 'Asylee', value: 'Asylee' },
                  { label: 'Resident Alien', value: 'resident_alien' },
                  { label: 'Other', value: 'other' },
                ],
                appearance: 'outline',
              },
            },
          ]
        },
        {
          template: `
            <hr />
            <p>When was your Visa, Permanent Resident or other issued? (optional)</p>
          `,
        },
        {
          fieldGroupClassName: 'display-flex',
          fieldGroup: [
            {
              type: 'datepicker',
              key: 'dateOfIssue',
              templateOptions: {
                label: 'Date of Issue',
                appearance: 'outline'
              },
            },
          ],
        },
      ]
    },
    {
      label: 'Military',
      header: 'Let us know if you have a military background',
      fields: [
        {
          template: `
            <p>Are you a current or former member of the U.S. Armed Forces, or Independent, spouse or widow a member of the U.S. Armed Forces?</p>
            <p><i>If you're associated with the military, you may be eligible for special tuition rates.</i></p>
          `,
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              type: 'radio',
              key: 'militaryMember',
              templateOptions: {
                options: [
                  {
                    label: 'Yes ',
                    value: 'yes',
                  },
                  {
                    label: 'No',
                    value: 'no',
                  }
                ],
              },
            },
          ],
        },
        {
          template: `<hr><p>Are you employed as a civilian with the Department of Defense or the Coast Guard?</p>`,
        },
        {
          fieldGroupClassName: 'displayflex',
          fieldGroup: [
            {
              type: 'radio',
              key: 'militaryEmployedCivilian',
              templateOptions: {
                options: [
                  {
                    label: 'Yes ',
                    value: 'yes',
                  },
                  {
                    label: 'No',
                    value: 'no',
                  }
                ],
              },
            },
          ],
        },
      ]
    }
  ];

  form = new FormArray(this.steps.map(() => new FormGroup({})));
  options = this.steps.map(() => <FormlyFormOptions> {});

  prevStep(step) {
    this.activedStep = step - 1;
  }

  nextStep(step) {
    this.activedStep = step + 1;
  }

  selectActiveStep(event) {
    this.activedStep = event.selectedIndex;
  }

  submit() {
    console.log(JSON.stringify(this.model));
  }
}
